import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

export default function Header() {
  const totalMount = useSelector((state) => state.cart.totalMount);
  return (
    <div className="p-5 row g-0 text-center">
      <div className="navBarShop col-6">
        <nav className="navbar navbar-expand-lg bg-body-tertiary">
          <div className="container-fluid">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link active mx-3" to="/">
                  PhoneShopPage
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link  mx-3" to="/detail/1">
                  DetailPhonePage
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link  mx-3" to="/cart">
                  CartPhonePage
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <div className="searchPhone col-4">
        <nav className="navbar bg-body-tertiary">
          <div className="container-fluid">
            <form className="d-flex" role="search">
              <input
                className="form-control me-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button className="btn btn-outline-success" type="submit">
                Search
              </button>
            </form>
          </div>
        </nav>
      </div>
      <span className="fa fa-cart-plus placeholder col-2">
        <strong>{totalMount}</strong>
      </span>
      {/* <div className="cartPhone col-4">
          <span className="fa fa-shopping-cart">
            Cart <strong>0</strong>
          </span>
        </div> */}

      {/* <NavLink className="px-3 py-1 rounded bg-success mx-3" to="/">
          PhoneShopPage
        </NavLink>
        <NavLink className="px-3 py-1 rounded bg-success mx-3" to="/detail/1">
          DetailPhonePage
        </NavLink>
        <NavLink className="px-3 py-1 rounded bg-success mx-3" to="/cart">
          CartPhonePage
        </NavLink>
         */}
    </div>
  );
}
