import PhoneShopPage from './Pages/PhoneShopPage/PhoneShopPage';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { DetailPhonePage } from './Pages/DetailPhonePage/DetailPhonePage';
import CartPhonePage from './Pages/CartPhonePage/CartPhonePage';
import Header from './Components/Header/Header';

function App() {
  return (
    <div className="App container">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<PhoneShopPage />}></Route>
          <Route path="/detail/:id" element={<DetailPhonePage />}></Route>
          <Route path="/cart" element={<CartPhonePage />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
