import React from 'react';
import { useParams } from 'react-router';
import { useSelector } from 'react-redux';
import { Card } from 'antd';

const { Meta } = Card;
export function DetailPhonePage() {
  let params = useParams();

  const detailPhone = useSelector((state) => state.cart.detailPhone);

  let renderPhoneDetail = () => {
    let { img, desc, name } = detailPhone;
    return (
      <div className="col-3 text-left flex" style={{ height: 500 }}>
        <Card
          hoverable
          style={{
            width: 240,
          }}
          cover={<img alt="example" src={img} />}>
          <Meta title={name} />
          <p className="card-desc">{desc}</p>
          <div className="flex">
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
              impedit fuga veritatis animi sunt tempora sed consequatur,
              mollitia eveniet cum!
            </p>
          </div>
        </Card>
      </div>
    );
  };

  return (
    <div>
      {params.id}
      {renderPhoneDetail()}
    </div>
  );
}
