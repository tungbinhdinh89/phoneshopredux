import React, { useState } from 'react';
import { Table } from 'antd';
import { columns } from '../ComponentsPhoneShop/Table';
import { useSelector, useDispatch } from 'react-redux';
import {
  handleDecreasement,
  handleIncreasement,
  handleDeleteCartItem,
} from '../../store/cartStore';

export default function CartPhonePage() {
  const [itemCourt, setItemCourt] = useState(0);

  const list = useSelector((state) => state.cart.cartItems);

  let totalMount = useSelector((state) => state.cart.totalMount);
  console.log('🚀 ~ totalMount:', totalMount);

  const dispatch = useDispatch();
  let cartItemCount = 0;
  console.log('🚀 ~ tungciunt:', cartItemCount);

  // renderCartItem
  let renderCartItem = () => {
    return list.map((cart, index) => {
      cartItemCount += cart.quantity;
      // totalMount = cartItemCount;

      let { id, name, quantity, price, img } = cart;
      return {
        key: index,
        id: id,
        name: name,
        img: <img style={{ height: 50 }} src={img} alt="" />,
        price: price,
        quantity: (
          <div>
            <button
              className="btn btn-danger"
              onClick={() => dispatch(handleDecreasement(id))}>
              -
            </button>
            <strong className="px-3">{quantity}</strong>
            <button
              className="btn btn-warning"
              onClick={() => dispatch(handleIncreasement(id))}>
              +
            </button>
          </div>
        ),
        action: (
          <div>
            <button
              className="btn btn-warning"
              onClick={() => dispatch(handleDeleteCartItem(id))}>
              Delete
            </button>
          </div>
        ),
      };
    });
  };

  return (
    <div className="coantainer">
      <h3 className="text-right">Total Mount: {totalMount}</h3>

      <Table columns={columns} dataSource={renderCartItem()} />
    </div>
  );
}
