import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  increasement,
  decreasement,
  changeQuantity,
} from '../../store/cartStore';

export default function CartPhonePage() {
  const list = useSelector((state) => state.cart.cartItems);
  const totalMount = useSelector((state) => state.cart.totalMount);

  const dispatch = useDispatch();
  let total = 0;

  return (
    <div className=" mx-auto">
      <table className="table">
        {list.map((item, index) => {
          total += item.quantity;
          return (
            <tbody key={index}>
              <tr>
                <td>Lorem ipsum dolor sit amet.</td>
                <td>123.</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => dispatch(decreasement(item.id))}>
                    -
                  </button>
                  <strong className="px-3">{item.quantity}</strong>
                  <button
                    className="btn btn-warning"
                    onClick={() => dispatch(increasement(item.id))}>
                    +
                  </button>
                </td>
                <td>
                  <button className="btn btn-warning mr-2">Edit</button>
                  <button className="btn btn-warning">Delete</button>
                </td>
              </tr>
            </tbody>
          );
        })}
      </table>

      <p className="text-right ">totalMount:{totalMount}</p>
    </div>
  );
}
