import React, { Component } from 'react';
import PhoneItem from './Components/PhoneItem';
import { useSelector, useDispatch } from 'react-redux';

export default function PhoneShopPage() {
  const phoneList = useSelector((state) => state.cart.phoneList);

  return (
    <div className="row">
      {phoneList.map((item, index) => {
        return <PhoneItem dataPhone={item} key={index} />;
      })}
    </div>
  );
}
