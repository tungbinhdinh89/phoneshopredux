import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { phoneList } from '../dataPhone';
import { Card } from 'antd';
import { handleAddToCart, handleDetailPhone } from '../../../store/cartStore';
const { Meta } = Card;

export default function PhoneItem({ dataPhone }) {
  const dispatch = useDispatch();
  let { name, img, desc } = dataPhone;
  return (
    <div className="col-3 text-left" style={{ height: 500 }}>
      <Card
        hoverable
        style={{
          width: 240,
        }}
        cover={<img alt="example" src={img} />}>
        <Meta title={name} />
        <p className="card-desc">{desc}</p>
        <div className="flex">
          <button
            className="btn btn-success mt-2"
            onClick={() => dispatch(handleAddToCart(dataPhone))}>
            Add To Cart
          </button>
          <button
            className="btn btn-warning mt-2"
            onClick={() => dispatch(handleDetailPhone(dataPhone.id))}>
            Watch Detail
          </button>
        </div>
      </Card>
    </div>
  );
}
