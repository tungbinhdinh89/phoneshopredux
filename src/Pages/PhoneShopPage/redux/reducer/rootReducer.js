import { combineReducers } from 'redux';
import { phoneShopReducer } from '../reducer/phoneShopReducer';

export const rootReducer = combineReducers(phoneShopReducer);
