import { createSlice } from '@reduxjs/toolkit';
import { phoneList } from '../Pages/PhoneShopPage/dataPhone';
const initialState = {
  phoneList: phoneList,
  detailPhone: phoneList[0],
  totalMount: 0,
  cartItems: [],
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    //
    // changeQuantity: (state, id, number) => {
    //   let cloneCart = [...state.cartItems];
    //   let index = cloneCart.findIndex((item) => {
    //     return id.payload === item.id;
    //   });
    //   cloneCart[index].quantity = cloneCart[index].quantity - number;
    //   state.cartItems = cloneCart;
    //   state.totalMount--;
    // },

    // increasement item

    handleIncreasement: (state, id) => {
      let cloneCart = [...state.cartItems];
      let index = cloneCart.findIndex((item) => {
        return id.payload === item.id;
      });
      cloneCart[index].quantity = cloneCart[index].quantity + 1;
      state.cartItems = cloneCart;
      state.totalMount = cloneCart[index].quantity;
    },
    // decreasement item
    handleDecreasement: (state, id) => {
      let cloneCart = [...state.cartItems];
      let index = cloneCart.findIndex((item) => {
        return id.payload === item.id;
      });
      cloneCart[index].quantity = cloneCart[index].quantity - 1;
      cloneCart[index].quantity == 0 && cloneCart.splice(index, 1);
      state.cartItems = cloneCart;
    },
    // deletet Cart
    handleDeleteCartItem: (state, id) => {
      // let cloneCart = [...state.cartItems];
      // cloneCart = cloneCart.filter((cart) => cart.id !== id.payload);
      // state.cartItems = cloneCart;
      let cloneCart = [...state.cartItems];
      let index = cloneCart.findIndex((item) => {
        return id.payload === item.id;
      });

      cloneCart.splice(index, 1);
      state.cartItems = cloneCart;
    },

    // detail phone
    handleDetailPhone: (state, phone) => {
      console.log('🚀 ~ state:', state);
      console.log('🚀 ~ phone:', phone);

      // state.detailPhone = phone.payload;
      console.log('🚀 ~ phone.payload:', phone.payload);
    },
    // add item to cart
    handleAddToCart: (state, phoneCart) => {
      console.log('🚀 ~ phoneCart:', phoneCart);

      let cloneCart = [...state.cartItems];
      let index = cloneCart.findIndex((item) => {
        return phoneCart.payload.id === item.id;
      });
      if (index === -1) {
        let newphoneCart = { ...phoneCart.payload, quantity: 1 };
        cloneCart.push(newphoneCart);
        state.cartItems = cloneCart;
      } else {
        cloneCart[index].quantity++;
        state.cartItems = cloneCart;
      }
    },
  },
});
export const {
  handleDecreasement,
  handleIncreasement,
  changeQuantity,
  handleAddToCart,
  handleDeleteCartItem,
  handleDetailPhone,
} = cartSlice.actions;
export default cartSlice.reducer;
